@ECHO OFF

pushd %~dp0

if "%1" == "" goto help
goto %1

:help
echo.Try one of the following targets as 'make ^<target^>':
echo.build install test doc
echo.
echo.Dependencies for your active Python environment:
echo.'build' requires 'flit'.
echo.'install' requires 'flit'.
echo.'test' requires 'pytest' and 'pytest-cov'.
echo.'doc' requires 'sphinx'.
goto end

:build
flit build --format wheel
goto end

:install
flit install
goto end

:test
pushd tests
py.test --cov=dash_project_layout --cov-report term-missing --junitxml=./.pytest_cache/pytest.xml
goto end

:doc
if exist ".\doc\source\reference\" rd /q /s ".\doc\source\reference\"
sphinx-apidoc -MTfo ./doc/source/reference ./dash_project_layout
./doc/make.bat html
goto end

:end
popd