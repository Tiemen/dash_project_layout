.PHONY: help build install test doc Makefile

# show some help
help:
	@echo "Try one of the following targets as 'make <target>':"
	@echo "build install test doc"
	@echo ""
	@echo "Dependencies for your active Python environment:"
	@echo "'build' requires 'flit'."
	@echo "'install' requires 'flit'."
	@echo "'test' requires 'pytest' and 'pytest-cov'."
	@echo "'doc' requires 'sphinx'."

build:
	flit build --format wheel

install:
	flit install

test:
	cd tests && py.test --cov=dash_project_layout --cov-report term-missing --junitxml=./.pytest_cache/pytest.xml

doc:
	rm -rf ./doc/source/reference
	sphinx-apidoc -MTfo ./doc/source/reference ./dash_project_layout
	cd doc && make html