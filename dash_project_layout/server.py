import dash
from dash_project_layout.layout.main import main

app = dash.Dash(__name__)
app.layout = main.layout
main.register(app)
run = app.run_server


def cli():
    """Dash server command line interface (CLI)."""
    import argparse

    parser = argparse.ArgumentParser(
        prog="Dash Project Layout", description="Example Dash Project Layout."
    )
    parser.add_argument("-d", "--debug", action="store_true", help="turn on debug mode")
    args = parser.parse_args()

    run(debug=args.debug)


if __name__ == "__main__":
    cli()
