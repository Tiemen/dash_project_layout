"""Plotly Dash app layout module."""
import dash_html_components as html
from dash import Dash


class Element:
    """Plotly Dash app element to standardize adding HTML divs and callbacks.

    Arguments:
        layout: HTML Div describing this element.
    """

    def __init__(self, layout: html.Div):
        self.layout = layout

    def register(self, app: Dash):
        """Register callbacks in app.

        Arguments:
            app: App to register callbacks in.

        Don't forget to add register calls of child elements!
        """
        raise NotImplementedError
