"""Main layout module."""

import dash_html_components as html
from dash import Dash
from dash.dependencies import Input, Output

from dash_project_layout.layout import Element

# Create layout here.
layout = html.Div(
    [
        html.Button("Click me", id="clickme-button"),
        html.Div("Waiting for a click...", id="content-div"),
    ],
    id="main-container",
    className="main-container",
)


# Add callbacks in derived class.
class MainElement(Element):
    def register(self, app: Dash):
        @app.callback(
            Output("content-div", "children"), [Input("clickme-button", "n_clicks")]
        )
        def content_div_children(n_clicks):
            if n_clicks < 10:
                return "{} clicks".format(n_clicks)
            return "Hello world!"


main = MainElement(layout)
