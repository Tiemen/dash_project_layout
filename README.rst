###################
Dash Project Layout
###################

Example Dash Project Layout.

***************
Developer guide
***************

==========
Versioning
==========

Versioning is performed based on git tags using the `versioneer <https://github.com/warner/python-versioneer/>`_ project.
See `_version.py <./test_project/_version.py>`_ in the source folder for the implementation.

Git tags need to follow a semantic versioning format (major.minor.patch), preceded by a ``v``, e.g. ``v0.0.1``.

============================
Python packaging information
============================

This project is packaged using `flit <https://flit.readthedocs.io/>`_. 
Packaging information as well as dependencies are stored in `pyproject.toml <./pyproject.toml>`_. 

Building the project (creating a wheel) can be done using ``make build``. 

Installing the project and its dependencies can be done using ``make install``. 

=======
Testing
=======

The tests need to be put in the `tests/ <./tests/>`_ directory  and can be run using ``make test`` on both Windows and Linux as long as `pytest <https://docs.pytest.org/>`_ and `pytest-cov <https://pytest-cov.readthedocs.io/>`_ are installed in the active Python environment.

=============
Documentation
=============

Building the documentation (in HTML) can be done using ``make doc``. 
The documentation is then built locally at `doc/build <./doc/build>`_ using `sphinx <http://www.sphinx-doc.org/>`_.
