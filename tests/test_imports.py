"""Import testing module."""


def test_dash_project_layout_imports():
    """Test imports in top-level."""

    import dash_project_layout
    import dash_project_layout.server

    del dash_project_layout
